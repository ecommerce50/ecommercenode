var express = require('express')
var app = express()
var bodyParser = require('body-parser');
const { response } = require('express');


app.use(bodyParser.urlencoded({ extended: true }));
app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*")
    res.setHeader("Access-Control-Allow-Headers", "*")
    res.setHeader("Access-Control-Allow-Methods", "*")
    next()
})

const DAO = require('@beweb/dao')
const uri = "mongodb+srv://yohan:123456waz@cluster0.7cwtk.mongodb.net/hsfeco?retryWrites=true&w=majority"
const db = new DAO.MongoDB(uri)

//Récuperation de tous les produits
app.get("/products",(req,res) => {
    db.getAll("hsfeco", doc => {
        res.setHeader("Content-type", "application/json")
        let code = 200
        let content = doc
        sendResponse(code,content,res)
    })   
})

//creer un nouveau produit
app.post("/newprod",(req,res)=> {  
    const newProd = {
        designation: req.body.designation,
        price: parseInt(req.body.price),
        quantity: parseInt(req.body.quantity)
    }
    db.create("hsfeco" , newProd)
    res.send(newProd) 
})

// app.put("/products/:id", (req,res) => {
//     let id = req.params.id
//     console.log(id);
//     let newprod = {
//         id: id,
//         designation: req.body.designation,
//         price: req.body.price,
//         stock: req.body.stock
//     }

//     let code = 200
//     let content = newProd
    
// })



//modification d'un produit
app.put("/product/:id", (req,res)=> {
    let id = req.params.id
    console.log(id);
    let code = 404
    let content = "Page non trouvée ..."
            let newProd = {
                id: id,
                designation: req.body.designation,
                price: parseInt(req.body.price),
                quantity:parseInt(req.body.quantity)
            }
                products.splice(prod,1,newProd)
                code = 200
                content = products
        
    sendResponse(code,content,res)
    })


//creer un nouveau panier vide
app.post("/newbask",(req,res)=>{
    let code = 500
    let content = "Problème avec le serveur"
    if(req.query.id){
        req.query.id = parseInt(req.query.id)
        var newBask = req.query
        baskets.push(newBask)
        code = 200
        content = "le panier à bien été crée"
    }else{
        code = 404
        content = "Veuillez entrer un identifiant..."
    }
    sendResponse(code,content,res)
})

//ajouter un produit à un panier
app.post("/add/baskets/:bid/prod/:pid",(req,res)=> {
    let bid = parseInt(req.params.bid)
    let pid = parseInt(req.params.pid)
    let bask, prod
    baskets.forEach(element => {
        if(element.id == bid){
            bask=element
        }
    });
    products.forEach(product => {
        if(product.id == pid){
            prod=product
        }
    });
    bask.items.push(prod)
    res.send(bask)
})


//nous renvois tous les paniers 
app.get("/baskets",(req,res) =>{
    let code = 404
    let content = "il n'y a pas de panier"
    if(baskets){
        code = 200
        content = baskets
    }
    sendResponse(code,content,res)
})

//nous renvois le contenu d'un panier selon son id que l'on passe en paramètre de la requête
app.get("/baskets/:id",(req,res)=> {
    let code = 404
    let content = "panier non trouvé"
        baskets.forEach(element => {
            if(req.params.id == element.id){
                code = 200
                content = element
        }
    });
    sendResponse(code,content,res)
}) 

//récupération des paniers validés pas le client
app.get("/client/:id",(req,res)=> {
    console.log(req.params.id);
    //on part du principe que la fonction ne va pas marcher
    //on lui assigne les valeurs de ce cas 
    let code = 404;
    let content = "client non trouvé"    
    baskets.forEach(element => {
        if(element.refClient == req.params.id && element.validitee){
            //si tout est ok on change juste nos variables et la réponse se génére
            code = 200
            content = element
        }
    });
    //Envois de la reponse
    sendResponse(code,content,res)
})
//supprimer un panier
app.delete("/del/basket/:id",(req,res)=> {
    let code = 500
    let content = "erreur avec le server"
    baskets.forEach(element => {
        if(req.params.id == element.id){
            baskets.pop(element)
            code = 200
            content = baskets
        }
    });
    sendResponse(code,content,res)
})



//Fonction qui génére la réponse
function sendResponse(code, content, res){
    res.status(code).send(content);
}

app.listen(3333)